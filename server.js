const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
app.use(bodyParser.json());
app.use(cors());

const database = {
    contacts: [
        {
            id: '123',
            name: 'Jone',
            email: 'Jone@hotmail.com',
            wasAdd: new Date()
        },
        {
            id: '124',
            name: 'Cat',
            email: 'Cat@hotmail.com',
            wasAdd: new Date()
        },
        {
            id: '127',
            name: 'Dou',
            email: 'dou@hotmail.com',
            wasAdd: new Date()
        },
    ]
}

app.get('/', (req, res)=> { 
    res.send(database.contacts) 
})

app.post('/add', (req, res) => { 
    const {email, name } = req.body
    database.contacts.push({
        id: '125',
        name: name,
        email: email,
        wasAdd: new Date()
    })
    res.json(database.contacts[database.contacts.length-1]) 
})

app.delete('/delete', (req, res) => { 
    const contactToDelete = database.contacts.findIndex((contact, i) => {
        if(contact.id === '124'){
            return i
        }
    })
    database.contacts.splice(contactToDelete, 1);

    res.json(database) 
})

app.get('/contact/:id', (req, res)=> {
    const { id } = req.params;
    let found = false;
    database.contacts.forEach(contact => {
        if(contact.id === id){
            found = true;
            return res.json(contact);
        } 
    })
    if(!found){
        res.status(400).json('not found')
    }
})

app.put('/contact/:id', (req, res) => {
    const { name, email, id } = req.body;
    let found = false;
    database.contacts.forEach(contact => {
        if(contact.id === id){
            found = true;
            contact.name = name
            contact.email = email
            return res.json(contact);
        } 
    })
    if(!found){
        res.status(400).json('not found')
    }
})


app.listen(3000, ()=> {
    console.log('app is running on port 3000');
})


/*
/add contact --> POST = user
/delete contact --- DELETE = user
/contact/:contactId --> GET = user
/editContact --> PUT --> user
*/